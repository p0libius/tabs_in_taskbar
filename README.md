# Integrate windows taskbar with Firefox tabbar

* Uncheck **Open links in tabs instead of new windows** in Options
* Install [No Tabs extension](https://addons.mozilla.org/en-US/firefox/addon/no-tabs/)
> Note that **Customize** panel is not available when No tabs is enabled
* Go to [about:config](about:config) and change `toolkit.legacyUserProfileCustomizations.stylesheets` to **true**
* Paste [userChrome.css](userChrome.css) into `%AppData%/Roaming/Mozilla/Firefox/Profiles/_your profile name_/chrome`